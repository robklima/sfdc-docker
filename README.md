#sfdc-docker
A docker image for deploying to Salesforce using Apache Ant the Force.com Migration Tool.

##Contents
* Alpine Linux
* OpenJDK
* Apache Ant
* Force.com Migration Tool
